from django.shortcuts import render, get_object_or_404
from .models import TodoList, TodoItem
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, UpdateView, DeleteView

# Create your views here.


def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {'todo_lists': todo_lists}
    return render(request, 'todo_list_list.html', context)


def todo_list_detail(request, id):
    todo_lists = get_object_or_404(TodoList, id=id)
    todo_items = TodoItem.objects.filter(list=todo_lists)
    context = {'todo_lists': todo_lists, 'todo_items': todo_items}
    return render(request, 'todo_list_detail.html', context)


class TodoListCreateView(CreateView):
    model = TodoList
    fields = ['name']
    template_name = 'todo_list_create.html'
    
    def get_success_url(self):
        return reverse_lazy('todo_list_detail', kwargs={'id': self.object.id})
    

class TodoListUpdateView(UpdateView):
    model = TodoList
    fields = ['name']
    template_name = 'todo_list_update.html'
    pk_url_kwarg = 'id' 
    
    def get_success_url(self):
        return reverse_lazy('todo_list_detail', kwargs={'id': self.object.id})
    

class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = 'todo_list_delete.html'
    pk_url_kwarg = 'id'
    success_url = reverse_lazy('todo_list_list')


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = 'todo_item_create.html'
    fields = ['task', 'due_date', 'is_completed', 'list']

    def get_success_url(self):
        return reverse_lazy('todo_list_detail', kwargs={'id': self.object.list.id})
    
    # Override the get_context_data method to include additional context
    def get_context_data(self, **kwargs):
        # Call the base implementation to get a context dictionary
        context = super().get_context_data(**kwargs)

        # Add the 'todo_lists' variable to the context
        # This will contain all TodoList objects, which we will use in the template for the dropdown.
        context['todo_lists'] = TodoList.objects.all()

        # Return the updated context dictionary
        return context


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    fields = ['task', 'due_date', 'is_completed', 'list']
    template_name = 'todo_item_update.html'
    pk_url_kwarg = 'id'

    def get_success_url(self):
        return reverse_lazy('todo_list_detail', kwargs={'id': self.object.list.id})