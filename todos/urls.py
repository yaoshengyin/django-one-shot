from django.urls import path
from . import views

urlpatterns = [
    path('', views.todo_list_list, name='todo_list_list'),
    path('<int:id>/', views.todo_list_detail, name='todo_list_detail'),
    path('create/', views.TodoListCreateView.as_view(), name='todo_list_create'),
    path('<int:id>/edit/', views.TodoListUpdateView.as_view(), name='todo_list_update'),
    path('<int:id>/delete/', views.TodoListDeleteView.as_view(), name='todo_list_delete'),
    path('items/create/', views.TodoItemCreateView.as_view(), name='todo_item_create'),
    path('items/<int:id>/edit/', views.TodoItemUpdateView.as_view(), name='todo_item_update'),
]
